Django==3.1.3
django_heroku==0.3.1
gunicorn==20.1.0
pyglet==1.5.21
Pillow==9.0.0
pyephem==9.99
