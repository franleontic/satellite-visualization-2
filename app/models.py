from django.db import models

class Image(models.Model):

    latdegrees = models.IntegerField()
    latminutes = models.IntegerField()
    latseconds = models.IntegerField()
    londegrees = models.IntegerField()
    lonminutes = models.IntegerField()
    lonseconds = models.IntegerField()
    height = models.IntegerField()
    theta = models.DecimalField(max_digits=5, decimal_places=2)
    phi = models.DecimalField(max_digits=5, decimal_places=2)

