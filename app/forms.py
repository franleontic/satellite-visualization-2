from django import forms
from django.forms import ModelForm
from app.models import Image


class CreateImageForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(CreateImageForm, self).__init__(*args, **kwargs)
        self.fields['latdegrees'].label = 'Latitude [degrees/minutes/seconds]'
        self.fields['latdegrees'].initial = '45'
        self.fields['latminutes'].initial = '00'
        self.fields['latseconds'].initial = '00'
        self.fields['londegrees'].label = 'Longitude [degrees/minutes/seconds]'
        self.fields['londegrees'].initial = '15'
        self.fields['lonminutes'].initial = '00'
        self.fields['lonseconds'].initial = '00'
        self.fields['height'].label = 'Height [km]'
        self.fields['height'].initial = '100'
        self.fields['theta'].label = 'Theta (towards north angle)'
        self.fields['theta'].initial = '0'
        self.fields['phi'].label = 'Phi (towards east angle)'
        self.fields['phi'].initial = '0'

    class Meta:
        model = Image
        fields = '__all__'
