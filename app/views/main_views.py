from app.forms import *
from django.shortcuts import render, redirect
from django.views import View
import subprocess


class HomeView(View):
    def get(self, request):
        context = {}
        form = CreateImageForm(request.POST or None)
        context['form'] = form
        return render(request, 'index.html', context)

    def post(self, request):
        context = {}
        form = CreateImageForm(request.POST or None)
        if form.is_valid():
            lat = float(form['latdegrees'].value()) + int(form['latminutes'].value()) / 60 + int(form['latseconds'].value()) / 3600
            lon = float(form['londegrees'].value()) + int(form['lonminutes'].value()) / 60 + int(form['lonseconds'].value()) / 3600
            height = form['height'].value()
            theta = form['theta'].value()
            phi = form['phi'].value()
            subprocess.call("python satvis.py " + str(lat) + " " + str(lon) + " " + height + " " + theta + " " + phi, shell=True)
            return redirect('/image')
        else:
            print(form.errors)
        return render(request, 'index.html', context)

class ImageView(View):
    def get(self, request):
        return render(request, 'satvis.html', {})
